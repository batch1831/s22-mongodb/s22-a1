// Users

{

	"_id" : "user001",
	"firstName" : "John",
	"lastName"	: "Smith",
	"email" : "johnsmith@mail.com",
	"password" : "johnsmith123",
	"isAdmin" : true,
	"mobileNumber" : "09123456789",
	"dateTimeRegistered" : "2022-06-10T15:00:00.00Z"

}

// Products

{

	"_id" : "product001",
	"name" : "iWatch",
	"description" : "Smart Watch",
	"price" : 5000,
	"stocks" : 6,
	"isActive" : true,
	"sku" : "ZZZZZ-12345",
	"dateTimeCreated" : "2022-06-10T15:00:00.00Z"
	
}

// Order Products

{

	"_id" : "OrderProducts001",
	"orderID" : "Orders001",
	"productID" : "product001",
	"quantity" : 1,
	"price" : 5000,
	"subtotal" : 5000,
	"dateTimeCreated" : "2022-06-10T15:00:00.00Z"
	
}

// Orders

{

	"_id" : "Orders001",
	"userID" : "user001",
	"transactionDate" : "2022-06-10T15:00:00.00Z",
	"status" : false,
	"total" : 1,
	"dateTimeCreated" : "2022-06-10T15:00:00.00Z"
	
}